#!usr/bin/env python

"""
Sandpit play ground for sending packets and reading icmp responses.

Lots of the code grabbed from web examples.

Author: M Campbell.  
Date: 2Jul2015

Source Control: https://bitbucket.org/whariki/mysandpit
"""

import socket
import os
import ctypes
import struct
import threading
import time

# host to listen
HOST = socket.gethostbyname(socket.gethostname())
print("Using IP Address: {}".format(HOST))

# from RFC792
ICMP_ECHOREPLY = 0
ICMP_DESTUNREACH = 3
ICMP_SOURCESQUELCH = 4
ICMP_REDIECT = 5
ICMP_ECHO = 8
ICMP_TIMEOUT = 11
ICMP_PAPAERROR = 12
ICMP_TIMESTAMP = 13
ICMP_TIMESTAMPREPLY = 14
ICMP_INFORMATIONREQ = 15
ICMP_INFORMATIONREPLY = 16

ICMP_TYPES = { \
    ICMP_ECHOREPLY:'Echo Reply', \
    ICMP_DESTUNREACH:'Destination Unreachable', \
    ICMP_SOURCESQUELCH:'Source Quench', \
    ICMP_REDIECT:'Redirect', \
    ICMP_ECHO:'Echo', \
    ICMP_TIMEOUT:'Time Exceeded', \
    ICMP_PAPAERROR:'Parameter Problem', \
    ICMP_TIMESTAMP:'Timestamp', \
    ICMP_TIMESTAMPREPLY:'Timestamp Reply', \
    ICMP_INFORMATIONREQ:'Information Request', \
    ICMP_INFORMATIONREPLY:'Information Reply' \
    }

ICMP_CODES = { \
    ICMP_DESTUNREACH : { \
    0:'net unreacable', \
    1:'host unreachable', \
    2:'protocol unreachable', \
    3:'port unreachable', \
    4:'fragmentation needed and DF bit set', \
    5:'source route failed'}, \
    ICMP_REDIECT:{ \
    0:'redirect datagrams for the network', \
    1:'redirect datagrmas for the host', \
    2:'redirect datagrams for the TOS and network', \
    3:'redirect datagrams for the TOS and host'}, \
    ICMP_TIMEOUT:{ \
    0:'ttl exceeded in transit', \
    1:'fragmentation reassembly time exceeded'} \
    }

class ICMP_response(Exception):
    def __init__(self,icmp_type,icmp_code,sender):
        self.icmp_type = icmp_type
        self.icmp_code = icmp_code
        self.sender = sender
        Exception.__init__(self,"An ICMP message has been recieved - Type: {}, Code: {}, From: {}".format(icmp_type,icmp_code,sender))

class ICMP(ctypes.Structure):
    """
    ICMP ctypes structure.  Helps decode ICMP messages
    """
    _fields_ = [
    ('type',        ctypes.c_ubyte),
    ('code',        ctypes.c_ubyte),
    ('checksum',    ctypes.c_ushort),
    ('para_0',      ctypes.c_ubyte),
    ('para_1',      ctypes.c_ubyte),
    ('para_2',      ctypes.c_ubyte),
    ('para_3',      ctypes.c_ubyte),
    ]

    def __new__(self, socket_buffer):
        return self.from_buffer_copy(socket_buffer)

    def __init__(self, socket_buffer):
        self.seq = None
        self.ident = None
        self.next_hop_mtu = None
        self.redirect = None
        self.pointer = None
        pass

    def decode(self):
        if self.type in ICMP_TYPES:
            tmp = '{}'.format(ICMP_TYPES[self.type])
            if self.type in ICMP_CODES:
                if ICMP_CODES[self.type].has_key(self.code):
                    tmp = '{}: {}'.format(tmp, ICMP_CODES[self.type][self.code])
                else:
                    tmp = '{}: Unknown code {}'.format(tmp, self.code)
        else:
            tmp = 'Unknown type {}: code {}'.format(self.type,self.code)

        if self.type in [ICMP_ECHO,ICMP_ECHOREPLY,ICMP_TIMESTAMP,ICMP_TIMESTAMPREPLY,ICMP_INFORMATIONREQ,ICMP_INFORMATIONREPLY]:
            self.seq = self.para_2 << 8 + self.para_3
            self.ident = self.para_0 << 8 + self.para_1
        elif self.type == ICMP_REDIECT:
            self.redirect = '{}.{}.{}.{}'.format(self.para_0,self.para_1,self.para_2,self.para_3)
            tmp = '{} to {}'.format(tmp,self.redirect)
        elif self.type == ICMP_PAPAERROR:
            self.pointer = self.para_0
            tmp = '{}: Parameter at pointer {}'.format(tmp, self.pointer)
        elif self.type == ICMP_DESTUNREACH & self.code == 4:
            # fragmentation not allowed
            self.next_hop_mtu = self.para_2 << 8 + self.para_3
            tmp = '{} (next hop MTU = {})'.format(tmp,self.next_hop_mtu)

        return tmp

class socket_withicmp(socket.socket):
    """
    Class provides a socket that creates an ICMP_response exception if an
    ICMP response is sent in return to a packet send.
    
    Extends the Socket.Socket interface by:
    1. Overriding the initialistion to bind the socket and optionally creates an ICMP monitoring thread
       so you don't need to use the new send method.
    2. Adds a send method that blocks for a timeout and waiting for an ICMP response to the sent packet.
    3. Overrides the socket close method to ensure that the ICMP thread is shut down if it was created.
    
    The threaded monitor part is still work in progress.
    """
    def __init__(self, host, port, socket_type = socket.SOCK_DGRAM, threaded_monitor=False):
        """
        Overrides the Socket.Socket initialise method and creates two sockets. 
        
        The socket that Socket.Socket creates, sets a timeout of 10 seconds and binds it 
        to the host and port specified.
        
        If threaded_monitor is true then it will start a general ICMP monitoring thread that
        will monitor for any ICMP messages.  You can then use the normal 'sendto' methods of 
        socket.socket and monitor for ICMP_response exceptions in the monitoring thread.  
        This part of the class is really work in progress.
        """
        # setup the main socket for application sending and receiving
        socket.socket.__init__(self, socket.AF_INET, socket_type)
        self.settimeout(10)
        self.bind((host,port))
        
        # create and start the icmp monitoring socket
        self.threaded_monitor = threaded_monitor
        if threaded_monitor:
            socket_protocol = socket.IPPROTO_ICMP
            self.icmp_socket = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket_protocol)
            self.icmp_socket.bind(( host, 0 ))
            self.icmp_socket.setsockopt(socket.IPPROTO_IP, socket.IP_HDRINCL, 1)

            if os.name == 'nt':
                self.icmp_socket.ioctl(socket.SIO_RCVALL, socket.RCVALL_ON)

            self.icmp_socket.settimeout(1)
            self._flush_icmp_recv()
            self._monitoring = False
            self._monitoring_thread = None
            self._monitor_for_icmp()

    def sendtowithicmpcheck(self, packet, address, icmp_timeout=2):
        """
        Adds a send method the socket.socket.sendto method to send and then wait (blocks)
        waiting for an icmp response
        """
        icmp_socket = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_ICMP)
        icmp_socket.setsockopt(socket.SOL_IP, socket.SO_REUSEADDR, 1)
        icmp_socket.bind((self.getsockname()[0],0))
        icmp_socket.setsockopt(socket.IPPROTO_IP, socket.IP_HDRINCL, 1)
        if os.name == 'nt':
            icmp_socket.ioctl(socket.SIO_RCVALL, socket.RCVALL_ON)
        icmp_socket.settimeout(icmp_timeout)
        self.icmp_type = None
        self.icmp_code = None

        #send the packet
        self.sendto(packet,address)
        t1 = t2 = time.time()

        #check for icmp response
        try:
            while icmp_timeout > t2 - t1:
                rsp, responder = icmp_socket.recvfrom(1600)
                t2 = time.time()
                # check rsp is from the sent packet.
                icmp_rsp = ICMP(rsp[20 : 20 + ctypes.sizeof(ICMP)])
                if icmp_rsp.type in [ICMP_DESTUNREACH,ICMP_TIMEOUT,ICMP_REDIECT,ICMP_PAPAERROR,ICMP_SOURCESQUELCH]:
                    iph = struct.unpack('!BBHHHBBH4s4s' , rsp[20 + ctypes.sizeof(ICMP): 40 + ctypes.sizeof(ICMP)])
                    if self.getsockname()[0] == socket.inet_ntoa(iph[8]) and socket.inet_ntoa(iph[9]) == address[0]:
                        self.icmp_type = icmp_rsp.type
                        self.icmp_code = icmp_rsp.code
                        print icmp_rsp.decode()
                        icmp_socket.close()
                        icmp_socket = None
                        return False
                timeout_tmp = icmp_timeout + t1 - time.time()
                if timeout_tmp > 0:
                    icmp_socket.settimeout(timeout_tmp)
                else:
                    icmp_socket.settimeout(0.001)

        except socket.timeout:
            pass
        icmp_socket.close()
        icmp_socket = None
        return True

    def _flush_icmp_recv(self):
        """
        Flushes the recev buffer of any packets.
        """
        tmp_timeout = self.icmp_socket.gettimeout()
        self.icmp_socket.settimeout(0.001)

        try:
            while True:
                dmp = self.icmp_socket.recvfrom(1600)
        except socket.timeout:
            self.icmp_socket.settimeout(tmp_timeout)
            return True

    def _monitor_for_icmp(self):
        """
        Starts a thread that monitors the port for icmp resposnes
        """
        self._monitoring = True
        self._monitoring_thread = threading.Thread(target=self._do_monitor,args=[threading.current_thread()])
        self._monitoring_thread.start()

    def close(self):
        """
        Overrides the socket.socket Close method to also close the icmp monitor.
        It waits for the icmp monitor thread to end.
        """
        # close icmp monitoring
        if self.threaded_monitor & self._monitoring:
            self.icmp_socket.close()
            self._monitoring = False
            #print("waiting for icmp thread to close")
            self._monitoring_thread.join()
            self.icmp_socket.close()

        socket.socket.close(self)


    def _do_monitor(self, parent_thread):
        # method to actually do the monitoring of icmp in a continuous loop
        while self._monitoring & parent_thread.isAlive():
            pkt_recv = False
            try:
                rsp = self.icmp_socket.recvfrom(1600)
                pkt_recv = True
            except socket.timeout:
                pkt_recv = False
                pass

            if pkt_recv:
                ip_header = rsp[0][0:20]
                iph = struct.unpack('!BBHHHBBH4s4s',ip_header)

                # Create the IP structure to get iph length and check it is ICMP
                version_ihl = iph[0]
                version = version_ihl >> 4
                ihl = version_ihl & 0xF
                iph_length = ihl * 4
                ttl = iph[5]
                protocol = iph[6]
                s_addr = socket.inet_ntoa(iph[8]);
                d_addr = socket.inet_ntoa(iph[9]);

                # print ('IP -> Version: {}, Header Length: {}, TTL: {}, Protocol: {}, Source: {}, Destination: {}'.format(str(version),str(ihl),str(ttl),str(protocol), str(s_addr),str(d_addr)))
                protocol
                # Create our ICMP structure
                buf = rsp[0][iph_length:iph_length + ctypes.sizeof(ICMP)]
                icmp_header = ICMP(buf)

                #print("ICMP -> Type:{}, Code:{}".format(icmp_header.type, icmp_header.code))

                raise ICMP_response(icmp_header.type, icmp_header.code,s_addr)
        #print('end of monitoring')


def main():
    s = socket_withicmp(HOST,50000)
    s.setsockopt(socket.IPPROTO_IP,socket.IP_TTL,1)
    #for i in range(5):
    #    s.setsockopt(socket.IPPROTO_IP,socket.IP_TTL,i+1)
    #    s.sendto("010101",('11.16.188.254',5000))
    #    time.sleep(2)
    s.sendtowithicmpcheck('012345',('11.16.188.254',5000),2)
    s.close()

if __name__ == '__main__':
    main()
