#!/usr/bin/env python
"""
parse_logs.py

Takes in a number of text files and parses them based on some rules...

This iteration takes logs from a DMR packet data tester and parses so the
results can be graphed.

21Jun2015 - MattC - initial value

"""
import sys
import getopt
import re
import os

TEST_HEADER = " Test: "
debug = True
WORKING_DIR = '/users/campbem/documents/enableip/performanceprofiling/DMR_packet_success (individual)/results/'
N = 100

def parse_filename(filename):
    filename = filename.replace('_','-')
    LL_type, FEC_rate, packet_size, fade_rate, signal_level = filename.split("-")[:5]

    if LL_type.lower() == 'c':
        LL_type = 'confirmed'
    elif LL_type.lower() == 'u':
        LL_type = 'unconfirmed'
    elif LL_type.lower() == 'uc':
        LL_type = None
    else:
        raise Exception("unknown LL_type {} from filename {}".format(LL_type,filename))

    if 'all' in FEC_rate.lower():
        FEC_rate = None
    elif FEC_rate == '1.0':
        FEC_rate = 'Full Rate'
    elif FEC_rate == '0.75':
        FEC_rate = '3/4 Rate'
    elif FEC_rate == '0.5':
        FEC_rate = '1/2 Rate'
    else:
        raise Exception('unknown FEC rate {} from filename {}'.format(FEC_rate,filename))

    if 'rang' in packet_size.lower() or '+' in packet_size:
        packet_size = None
    elif packet_size.lower() == '200b':
        packet_size = '200'
    else:
        raise Exception('unknown packet size {} from filename {}'.format(packet_size,filename))

    signal_level = "-" + signal_level

    return (LL_type, FEC_rate, packet_size, fade_rate, signal_level)


def normalise_test_parameters(tp):
    if "Half Rate" in tp:
        tp = tp[:tp.index("Half Rate")] + ("1/2 Rate",) + tp[tp.index("Half Rate")+1:]

    return tp

if __name__ == '__main__':
    try:
        opts, args = getopt.getopt(sys.argv[1:],"hf:r:",["files=","regex="])
    except getopt.GetoptError:
        print('{} -f <files> -r <regex>'.format(sys.argv[0]))
        sys.exit(2)

    files = os.listdir(WORKING_DIR)
    sys.stdout.write("trial number,call number,call setup (s), call time (s), call result, LL type, FEC rate, packet size, fade rate, signal level\n")

    trial_number = 0
    call_number = 0
    setup_time = 0
    call_time = 0
    call_result = "None"

    results_call_success_count = {}
    results_call_success = {}
    results_call_time = {}
    results_call_time_overall = {}
    results_stats_call_time = {}
    results_stats_call_success = {}

    for f in files:
        if ".txt" in f:
            try:
                test_parameters = parse_filename(f)
            except Exception as E:
                print E
            if debug: sys.stderr.write('{}\n'.format(test_parameters))
            fd = open(WORKING_DIR + f,'r')
            line = fd.readline()
            while line != '':
                if "Setup Call" in line:
                    trial_number += 1
                    call_number = 0
                    setup_time = 0
                    call_time = 0
                    call_result = "None"
                    while not "Setup Time:" in line and line != '':
                        line = fd.readline()
                    if not "Setup Time:" in line:
                        break
                    setup_time = line.rsplit(' ',1)[1].strip()
                    while not "Call Number" in line and line != '':
                        line = fd.readline()
                    if not "Call Number " in line:
                        break
                    if "FAIL" in line:
                        call_result = "FAIL"
                        call_number = line.partition("FAIL")[0].rsplit(' ',1)[1].strip()
                    elif "SUCCESS" in line:
                        call_result = "PASS"
                        call_number = line.partition("Number ")[2].split(' ',1)[0]
                    while not "Call Time" in line and line != '':
                        line = fd.readline()
                    if not "Call Time" in line:
                        break
                    call_time = line.rsplit(' ',1)[1].strip()

                    sys.stdout.write('{},{},{},{},{},{},{},{},{},{}\n'.format( \
                        trial_number, \
                        call_number, \
                        setup_time, \
                        call_time, \
                        call_result, \
                        test_parameters[0], \
                        test_parameters[1], \
                        test_parameters[2], \
                        test_parameters[3], \
                        test_parameters[4]))

                    if not test_parameters in results_call_success:
                        results_call_success[test_parameters] = {}
                        results_call_time[test_parameters] = {}
                        results_call_success_count[test_parameters] = (0,0)
                        results_call_time_overall[test_parameters] = (0,0)
                        #results_stats_call_time[test_parameters] = {'population':{'N':0,'total_time':0}}
                        #results_stats_call_success[test_parameters] = {}

                    results_call_success[test_parameters][call_number] = call_result
                    results_call_time[test_parameters][call_number] = call_time
                    if call_result == "PASS":
                        results_call_success_count[test_parameters] = (results_call_success_count[test_parameters][0] + 1,results_call_success_count[test_parameters][1]+1)
                        results_call_time_overall[test_parameters] = (results_call_time_overall[test_parameters][0]+float(call_time),results_call_time_overall[test_parameters][1]+1)

                        #trial_number = int(results_call_success_count[test_parameters][1] / N)
                        #if not trial_number in results_stats_call_success[test_parameters]:
                        #    results_stats_call_success[test_parameters][trial_number] = {'PASS':0,'N':0}
                        #sample_number_in_trial = results_call_success_count[test_parameters][1] % N
                        #results_stats_call_success[test_parameters][trial_number] = {'PASS':results_stats_call_success[test_parameters][trial_number]['PASS']+1,'N':sample_number_in_trial}
                        #
                        #trial_number = int(results_call_success_count[test_parameters][0] / N)
                        #if not trial_number in results_stats_call_time:
                        #    results_stats_call_time[test_parameters][trial_number] = {'N':0,'total_time':0,'variance**2':0}
                        #results_stats_call_time[test_parameters][trial_number]['N'] = results_call_success_count[test_parameters][0] % N
                    else:
                        results_call_success_count[test_parameters] = (results_call_success_count[test_parameters][0],results_call_success_count[test_parameters][1]+1)

                if "***** Test: " in line:
                    tmp = (line.split('Test:')[1].split(',',1)[0].strip().lower(),)
                    tmp += (line.split(',',2)[1].strip(),)
                    tmp += (line.split('=')[1].strip().split(' ')[0],)
                    test_parameters = tmp + test_parameters[3:]
                    test_parameters = normalise_test_parameters(test_parameters)
                    if debug: sys.stderr.write('{}\n'.format(test_parameters))
                if "Payload: " in line:
                    if test_parameters[2] is None:
                        test_parameters_tmp = test_parameters[0:2] + (line.split("Payload: ")[1].split(" ",1)[0], ) + test_parameters[3:]
                        if debug: sys.stdout.write('replacing {} with {}\n'.format(test_parameters,test_parameters_tmp))
                        results_call_success[test_parameters_tmp] = results_call_success.pop(test_parameters)
                        results_call_time[test_parameters_tmp] = results_call_time.pop(test_parameters)
                        results_call_success_count[test_parameters_tmp] = results_call_success_count.pop(test_parameters)
                        results_call_time_overall[test_parameters_tmp] = results_call_time_overall.pop(test_parameters)
                        test_parameters = test_parameters_tmp

                line = fd.readline()
            fd.close()
    for t in results_call_success_count:
        tmp = results_call_success_count[t]
        tmp2 = results_call_time_overall[t]
        if tmp[1] == 0:
            sys.stderr.write('no results counted for {0.000}\n'.format(t))
        if tmp2[1] == 0:
            tmp3 = '-'
        else:
            tmp3 = tmp2[0]/tmp2[1]
        sys.stdout.write('Overall pass % for {},{}, Successful Call time (s),{}\n'.format(t,float(tmp[0])/float(tmp[1])*100,tmp3))
