#!/usr/bin python

"""
sim_timers.py

Module to provide a simulator based set_time_out, clear_time_out

"""

import logging
log = logging.getLogger('sim_timers')
logFileHandler = logging.FileHandler('./output.log')
logFileHandler.setFormatter(logging.Formatter('%(asctime)s, %(levelname)s, %(name)s, %(funcName)s, %(message)s'))
log.addHandler(logFileHandler)
log.setLevel(logging.DEBUG)

SIM_TIMER_RESOLUTION_TICKS_PER_SECOND = 2000

class SimulatorEventListError(Exception):
    def __init__(self, *args):
        log.error('Exception raised: {}'.format(self.message))
        super(Exception, self).__init__()

class SimulatorEventList(object):
    """class SimulatorEventList

    {
        previous_event: SimulatorEventList,
        time: n,
        event_id: n,
        event: (callback, *callbackargs, **event_kwargs),
        next_event: SimulatorEventList
    }

    The class is an item in an ordered linked list of events, an event
    callback may input an event into the future.

    """
    next_event_id = 0
    #event_times = []
    current_event = None

    def __init__(self, time = 0, event_cb = None, event_args = [], event_kwargs={}, allow_inserts_in_past = False):
        super(SimulatorEventList, self).__init__()
        log.debug("attempting to insert event")
        if SimulatorEventList.current_event is None:
            SimulatorEventList.current_event = self
            self._time = time

        if time > 0 and time < SimulatorEventList.current_event.time:
            if allow_inserts_in_past:
                SimulatorEventList.current_event = self
                log.debug("reseting the simulator time as we are inserting an event in the past")
            else:
                raise SimulatorEventList("Creating event for time {} when current_event {}".format)

        if time == 0:
            self._time = SimulatorEventList.current_event.time;
        else:
            self._time = time

        self._event_id = SimulatorEventList.next_event_id
        SimulatorEventList.next_event_id += 1

        self._event = (event_cb, event_args, event_kwargs)
        self._event_done = False
        self._previous_event = None
        self._next_event = None

        # find where to insert
        insert_after_event = SimulatorEventList.current_event
        if self._event_id != insert_after_event.event_id:
            while insert_after_event.next_event is not None and insert_after_event.next_event.time <= self._time:
                insert_after_event = insert_after_event.next_event

            if insert_after_event.time > self._time:
                log.debug("attempting to insert event into the past")
                # search the past times if we are allowed to insert in the past
                while insert_after_event.previous_event is not None and insert_after_event.previous_event.time > self._time:
                    insert_after_event = insert_after_event.previous_event

            # do the insert
            self._next_event = insert_after_event.next_event
            self._previous_event = insert_after_event
            insert_after_event.next_event = self
            if self._next_event is not None:
                self._next_event.previous_event = self

        log.debug("inserted event {} into event queue at time {}, {}".format( \
            self._event_id,self._time, self))


    @property
    def next_event(self):
        """The next SimulatorEventList in time"""
        return self._next_event

    @next_event.setter
    def next_event(self, next_event):
        if next_event.time >= self.time:
            self._next_event = next_event
        else:
            raise SimulatorEventListError("Attempting to insert event with time {} before time {}".format(\
                self.time, next_event.time))

    @property
    def previous_event(self):
        """The previous SimulatorEventList in time"""
        return self._previous_event

    @previous_event.setter
    def previous_event(self, previous_event):
        if previous_event.time <= self.time:
            self._previous_event = previous_event
        else:
            raise SimulatorEventListError("Attempting to insert event with time {} after time {}".format(\
                self.time, previous_event.time))

    @property
    def time(self):
        '''
        The time associated to this event list.
        '''
        return self._time

    @property
    def event_id(self):
        '''
        The event_id.
        '''
        return self._event_id

    @property
    def event(self):
        '''
        The event callback, the agrs and kwargs
        '''
        return self._event

    @property
    def event_done(self):
        '''
        Flag showing if this event has been triggered
        '''
        return self._event_done

    @event_done.setter
    def event_done(self, done):
        self._event_done = done

    def __iter__(self):
        return SimulatorEventList.current_event
    def next(self):
        if self.next_event is None:
            raise StopIteration
        else:
            return self.next_event

    def do_event(self):
        '''
        initiates the event by running the callback.

        blocks until the callback has returned.
        '''
        log.debug("initiating event event_id={}: {}".format(self.event_id, self.event))
        return_val = None
        if not self._event_done:
            return_val = self._event[0](*self._event[1],**self._event[2])
            self._event_done = True
            log.debug("event complete event_id={}, returned {}".format(self.event_id, return_val))
        else:
            log.debug("event_id={} already marked as done".format(self.event_id))

        return return_val

    def remove_event(self):
        '''
        removes the event from the linked list
        '''
        log.debug('removing the event_id={}'.format(self.event_id))
        if SimulatorEventList.current_event.event_id == self._event_id:
            raise SimulatorEventListError('Cannot remove event that is the current_event')
        self._next_event.previous_event = self._previous_event
        self._previous_event.next_event = self._next_event

class SimulationTimer(object):
    """
    The simulation timer object that keeps track of events in time.

    On creation the simulation is not running, but there is a single event
    queued at the time given for maximum time that will stop the simulation.
    The default for maximum time is 24 hours in SIM_TIMER_RESOLUTION_TICKS_PER_SECOND
    ticks, ie 24 * 60 * 60 seconds.

    If the start_simulation method is called with no further events inserted
    then the first event to be executed will be the one that stops the simulation.

    The usual workflow for a simulation is:
    1) create a single Simulator Timer instance
    2) setup the simulation by inserting events into the simulation timeline,
       e.g. SimulationTimer.insert_event(0, initial_event, [arg1, arg2], {"kw1":kwarg1})
    3) start the simulation, SimulationTimer.start()
    """
    def __init__(self, max_time = 24 * 60 * 60):
        super(SimulationTimer,self).__init__()
        self._running = False
        self._max_time = max_time
        self._timeline = SimulatorEventList(0, log.debug, ["starting simulation"], allow_inserts_in_past=True)
        self.set_timeout(max_time, self.stop,[],{})

    @property
    def running(self):
        '''
        state of the simulation
        '''
        return self._running

    def set_timeout(self, timeout_s, callback, args, kwargs):
        '''
        Sets an event at some point in the future, and returns an event id
        '''
        event_time = SimulatorEventList.current_event.time + timeout_s * SIM_TIMER_RESOLUTION_TICKS_PER_SECOND
        return self.insert_event(event_time, callback, args, kwargs)

    def insert_event(self, time, callback, args, kwargs):
        '''
        inserts an event at a particular time (tick).

        Raises an error if attempting to insert before the current_event
        '''
        event = SimulatorEventList(time, callback, args, kwargs, not self.running)
        return event

    def start(self):
        '''
        Starts the simulation loop

        Ends when there are no more events in the queue or one of the events calls
        stop()
        '''
        self._running = True
        event = self._timeline.current_event
        while self._running and event is not None:
        #for event in self._timeline:
            log.debug('initiating event_id={}'.format(event.event_id))
            res = event.do_event()
            log.debug('completed event_id={} with result {}'.format(event.event_id,res))
            event = event.next_event
            #if not self._running:
            #    break

        self.stop()

    def stop(self):
        '''
        Stops the simulation loop
        '''
        self._running = False

    def now(self):
        '''
        returns the current simulation time
        '''
        self._timeline.current_event.time
