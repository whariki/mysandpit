from behave import *
import sim_timers
import time as t

"""
Feature: Simulator timer functions

    Scenario Outline: Insert an event into a simulated timeline.
        Given a simulator is running
        When hello world event is scheduled for time "1"
        Then the hello world event occurs at time "1"

    Scenario Outline: Insert two events into a simulated timeline.
        Given a simulator is running
        When hello world event is scheduled for time "4"
        When hello world-2 event is scheduled for time "2"
        Then the hello world-2 event occurs at time "2"
        Then the hello world event occurs at time "4"
"""

def hello_world_event(context):
    print ('hello world event {0} at {1}'.format(\
        context.hello_world_event_id, context.simulator.now()))

def hello_world_event_2(context):
    print ('hello world event 2 {0} at {1}'.format(\
        context.hello_world_event_2_id, context.simulator.now()))

@given('a simulator is running')
def step_impl(context):
    context.simulator = sim_timers.SimulationTimer()

@when('hello world event is scheduled for time "{time}"')
def step_impl(context, time):
    context.hello_world_event_id = context.simulator.insert_event(time,hello_world_event,[context],{})

@when('hello world-2 event is scheduled for time "{time}"')
def step_impl(context, time):
    context.hello_world_event_2_id = context.simulator.insert_event(time,hello_world_event_2,[context],{})

@when('the simulation is started')
def step_impl(context):
    context.simulator.start()

@then('the hello world event occurs at time "{time}"')
def step_impl(context,time):
    while context.simulator.now() <= time:
        t.sleep(1)
        pass
    assert 'hello world event {0} at {1}'.format( \
        context.hello_world_event_id, time) \
        in context.stdout_capture.getvalue()

@then('the hello world-2 event occurs at time "{time}"')
def step_impl(context,time):
    while context.simulator.now() <= time:
        t.sleep(1)
        pass
    assert 'hello world event 2 {0} at {1}'.format( \
        context.hello_world_event_2_id, time) \
        in context.stdout_capture.getvalue()
