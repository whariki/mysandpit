Feature: Simulator timer functions

    Scenario: Insert an event into a simulated timeline.
        Given a simulator is running
        When hello world event is scheduled for time "1"
        When the simulation is started
        Then the hello world event occurs at time "1"

    Scenario: Insert two events into a simulated timeline.
        Given a simulator is running
        When hello world event is scheduled for time "4"
        When hello world-2 event is scheduled for time "2"
        When the simulation is started
        Then the hello world-2 event occurs at time "2"
        Then the hello world event occurs at time "4"
