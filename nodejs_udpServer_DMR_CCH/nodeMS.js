#!/usr/bin/nodejs
// sends stuff to the server

const common = require('./nodeCommonResource.json');

const date = new Date();

// original structure based on example at https://nodejs.org/api/dgram.html
const dgram = require('dgram');
const ms = dgram.createSocket('udp4');

ms.on('error', (err) => {
  console.log(`MS error:\n${err.stack}`);
  ms.close();
});

ms.on('message', (msg, rinfo) => {
  rinfo.arrival_time = date.getTime();
  console.log(`MS got: ${msg} from ${rinfo.address}:${rinfo.port} at ${rinfo.arrival_time}`);

  ms.close();
});

var seq_number = 1;
var msg = seq_number.toString(16) + ':' + date.getTime().toString();
var msg_buf = Buffer(msg,common.buffer_encoding);
console.log(`MS sent: ${msg_buf} to ${common.server_hostname}:${common.server_port}`);
ms.send(msg_buf, 0, msg_buf.length, common.server_port, common.server_hostname, (err) => {
    console.log(err);
});

//ms.close();
