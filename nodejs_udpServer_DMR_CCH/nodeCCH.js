#!/usr/bin/nodejs
// DMR CCH control channel simulator

const common = require('./nodeCommonResource.json');

const date = new Date();

// original structure based on example at https://nodejs.org/api/dgram.html
const dgram = require('dgram');
const server = dgram.createSocket('udp4');

server.on('error', (err) => {
  console.log(`server error:\n${err.stack}`);
  server.close();
});

server.on('message', (msg, rinfo) => {
  rinfo.arrival_time = date.getTime();
  console.log(`server got: ${msg} from ${rinfo.address}:${rinfo.port} at ${rinfo.arrival_time}`);

  // echo back message
  setTimeout( (msg, rinfo, arrval_time) => {
      var new_msg = msg.toString(common.buffer_encoding) + ':' + rinfo.arrival_time.toString() + ':' + date.getTime().toString();
      // var new_msg = msg.toSting('hex') + rinfo.arrval_time.toString(16) + date.getTime().toString(16);

      var msg_buf = Buffer(new_msg,common.buffer_encoding);
      server.send(msg_buf, 0, msg_buf.length, rinfo.port, rinfo.address, (err) => {
          console.log(err);
      });

      console.log(`sent: ${msg_buf} to ${rinfo.address}:${rinfo.port} at ${date.getTime()}`);
  }, 60, msg, rinfo);
});

server.on('listening', () => {
  var address = server.address();
  console.log(`server listening ${address.address}:${address.port}`);
});

server.bind(common.server_port, common.server_hostname);
// server listening 0.0.0.0:41234
